FROM eclipse-temurin:21-jre-noble
LABEL maintainer="tom.gilissen@naturalis.nl"
COPY target/infuser.jar /home/app/infuser.jar
COPY target/libs /home/app/libs
RUN mkdir -p /data/jobs \
 && mkdir -p /data/files \
 && mkdir -p /data/archive
WORKDIR /home/app

RUN echo '#!/bin/bash' > /home/app/run.sh \
 && echo 'java -cp ./libs:./infuser.jar nl.naturalis.colander.infuser.Loader' >> /home/app/run.sh \
 && chmod a+x run.sh

CMD ["./run.sh"]

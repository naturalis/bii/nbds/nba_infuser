# README

## NBA Infuser

The NBA Infuser processes JSON documents 
and reads them into the document store 
that serves as the basis for the Naturalis Biodiversity API.

Running the NBA Infuser requires some variables
that need to be set as environment variables.
Checkout `nba.tpl` for the variables that need to be set.


## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`

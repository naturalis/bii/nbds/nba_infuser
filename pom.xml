<project xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>nl.naturalis.colander</groupId>
  <artifactId>infuser</artifactId>

  <packaging>jar</packaging>

  <version>1.5</version>
  <name>NBA Infuser</name>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

    <java.version>21</java.version>
    <elasticsearch.version>7.12.1</elasticsearch.version>
    <jackson.version>2.9.8</jackson.version>
    <log4j.version>2.17.1</log4j.version>
    <minio.version>6.0.2</minio.version>
    <junit.version>4.12</junit.version>

    <maven.compiler.plugin.version>3.8.0</maven.compiler.plugin.version>
    <maven.plugin.version>3.1.1</maven.plugin.version>
  </properties>

  <dependencies>

    <dependency>
      <groupId>org.elasticsearch</groupId>
      <artifactId>elasticsearch</artifactId>
      <version>${elasticsearch.version}</version>
    </dependency>

    <dependency>
      <groupId>org.elasticsearch.client</groupId>
      <artifactId>elasticsearch-rest-client</artifactId>
      <version>${elasticsearch.version}</version>
    </dependency>

    <dependency>
      <groupId>org.elasticsearch.client</groupId>
      <artifactId>elasticsearch-rest-high-level-client</artifactId>
      <version>${elasticsearch.version}</version>
    </dependency>

    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
      <version>${jackson.version}</version>
    </dependency>

    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-core</artifactId>
      <version>${jackson.version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-api</artifactId>
      <version>${log4j.version}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
      <version>${log4j.version}</version>
    </dependency>

    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
      <version>2.6</version>
    </dependency>

    <dependency>
      <groupId>io.minio</groupId>
      <artifactId>minio</artifactId>
      <version>${minio.version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-text</artifactId>
      <version>1.8</version>
    </dependency>

    <dependency>
      <groupId>de.grundid.opendatalab</groupId>
      <artifactId>geojson-jackson</artifactId>
      <version>1.8.1</version>
    </dependency>

    <dependency>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-surefire-plugin</artifactId>
      <version>2.22.2</version>
    </dependency>

    <!-- TEST -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>${junit.version}</version>
      <scope>test</scope>
    </dependency>

  </dependencies>

  <build>
    <finalName>infuser</finalName>
    <plugins>

      <!-- Set a JDK compiler level -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>${maven.compiler.plugin.version}</version>
        <configuration>
          <release>${java.version}</release>
        </configuration>
      </plugin>

      <!-- Make this jar executable -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>${maven.plugin.version}</version>
        <configuration>
          <archive>
            <manifest>
              <addClasspath>true</addClasspath>
              <classpathPrefix>libs/</classpathPrefix>
              <mainClass>
                nl.naturalis.colander.infuser.Loader
              </mainClass>
            </manifest>
          </archive>
        </configuration>
      </plugin>

      <!-- Copy project dependency -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-dependency-plugin</artifactId>
        <executions>
          <execution>
            <id>copy-dependencies</id>
            <phase>prepare-package</phase>
            <goals>
              <goal>copy-dependencies</goal>
            </goals>
            <configuration>
              <outputDirectory>
                ${project.build.directory}/libs
              </outputDirectory>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <!-- The Surefire Plugin is used during the test phase of the build lifecycle
        to execute the unit tests of an application. -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.22.2</version>
        <configuration>
          <!-- <systemPropertyVariables></systemPropertyVariables> -->
          <environmentVariables>
            <NBA_HOST>localhost</NBA_HOST>
            <NBA_PORT>9200</NBA_PORT>
            <NBA_SCHEME>http</NBA_SCHEME>
            <INFUSER_JOBSPATH>/data/datasets</INFUSER_JOBSPATH>
            <INFUSER_INCOMINGPATH>/data/output</INFUSER_INCOMINGPATH>
            <INFUSER_ARCHIVEPATH>/data/archive</INFUSER_ARCHIVEPATH>
          </environmentVariables>
          <includes>
                        <include>**/*Test.java</include>
                    </includes>
        </configuration>
      </plugin>


    </plugins>
  </build>

</project>

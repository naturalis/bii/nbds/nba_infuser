package nl.naturalis.colander.infuser;

import java.util.Collection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BulkDeleter {

  private static final Logger logger = LogManager.getLogger(BulkDeleter.class);

  BulkDeleter() {}

  public void delete(Collection<String> documents) { }

  public void createIndex()  { }

  public void flush() { }

}

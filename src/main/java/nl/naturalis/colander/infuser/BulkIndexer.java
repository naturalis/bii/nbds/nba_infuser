package nl.naturalis.colander.infuser;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import nl.naturalis.colander.infuser.model.DocumentType;
import nl.naturalis.colander.infuser.model.GeoShape;
import nl.naturalis.colander.infuser.model.Job;
import nl.naturalis.colander.infuser.model.LoadError;
import nl.naturalis.colander.infuser.model.ParseError;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;

public class BulkIndexer {

  private static final Logger logger = LogManager.getLogger(BulkIndexer.class);
  private static final ObjectMapper objectMapper = new ObjectMapper();
  private final RestHighLevelClient client;
  private final String index;
  private final DocumentType documentType;
  private final Job job;
  //private File errorFile;

  BulkIndexer(RestHighLevelClient client, DocumentType documentType, String index, Job job) {
    this.client = client;
    this.index = index;
    this.documentType = documentType;
    this.job = job;
  }

  public String getIndex() {
    return index;
  }

  public DocumentType getDocumentType() {
    return documentType;
  }

  /**
   * Bulk index.
   *
   * @param documents  the documents to be indexed
   * @throws IOException when interacting with Elasticsearch fails
   */
  public void index(Collection<String> documents) throws IOException {

    StringBuilder buffer = new StringBuilder(1024 * 1024 * 16);
    BulkRequest bulkRequest = new BulkRequest();
    bulkRequest.timeout(TimeValue.ZERO);
    int parseErrors = 0;

    // Create the buffer: this means removing the id from the json string
    // and apply it to the header so the document get's indexed with the
    // given id
    for (String document : documents) {
      Map<String, Object> map;
      try {
        map = objectMapper.readValue(document, new TypeReference<Map<String, Object>>() {});
      } catch (JsonParseException e) {
        parseErrors++;
        logger.error("Failed to parse document. Reason: {}", e.getMessage());
        logger.error("Document was skipped");
        job.addComment("Parsing error in: ".concat(document).concat("\n"));
        ParseError error = new ParseError(documentType, document, e.getMessage());
        job.addParseError(documentType, error);
        job.addToTotalErrors(1);
        continue;
      }

      // Workaround to deal with escaped characters
      String id = StringEscapeUtils.escapeJson(map.get("id").toString());
      map.remove("id");

      // Add geoShape (gatheringEvents.siteCoordinates.geoShape)
      List<Object> siteCoordinatesUpdated;

      // Specimen : a specimen has just one gathering event
      if (LoaderConfig.getInstance().addGeoShape() && documentType.equals(DocumentType.SPECIMEN)) {
        Map<String, Object> gatheringEvent = (HashMap<String, Object>) map.get("gatheringEvent");
        siteCoordinatesUpdated = new ArrayList<>();
        if (gatheringEvent != null) {
          List<Object> siteCoordinates = (List<Object>) gatheringEvent.get("siteCoordinates");
          if (siteCoordinates != null) {
            for (Object siteCoordinate : siteCoordinates) {
              HashMap<String, Object> coordinate = (HashMap<String, Object>) siteCoordinate;
              if (coordinate.get("longitudeDecimal") != null
                  && coordinate.get("latitudeDecimal") != null) {
                if (coordinate.get("geoShape") == null) {
                  Double longitudeDecimal = null;
                  Double latitudeDecimal = null;
                  if (coordinate.get("longitudeDecimal") instanceof Double) {
                    longitudeDecimal = (Double) coordinate.get("longitudeDecimal");
                  } else if (coordinate.get("longitudeDecimal") instanceof Integer) {
                    longitudeDecimal = ((Integer) coordinate.get("longitudeDecimal")).doubleValue();
                  }
                  if (coordinate.get("latitudeDecimal") instanceof Double) {
                    latitudeDecimal = (Double) coordinate.get("latitudeDecimal");
                  } else if (coordinate.get("latitudeDecimal") instanceof Integer) {
                    latitudeDecimal = ((Integer) coordinate.get("latitudeDecimal")).doubleValue();
                  }
                  if (longitudeDecimal != null && latitudeDecimal != null) {
                    GeoShape geoShape = new GeoShape(longitudeDecimal, latitudeDecimal);
                    coordinate.put("geoShape", geoShape);
                  }
                }
              }
              siteCoordinatesUpdated.add(coordinate);
            }
          }
        }
        if (siteCoordinatesUpdated.size() > 0) {
          gatheringEvent.put("siteCoordinates", siteCoordinatesUpdated);
          map.put("gatheringEvent", gatheringEvent);
        }
      }

      // Multimedia : a multimedia object can have one or more gathering events
      if (LoaderConfig.getInstance().addGeoShape()
          && documentType.equals(DocumentType.MULTIMEDIAOBJECT)) {
        if (map.get("gatheringEvents") == null) {
          continue;
        }
        List<Map<String, Object>> gatheringEvents =
            (List<Map<String, Object>>) map.get("gatheringEvents");
        List<Map<String, Object>> gatheringEventsUpdated = new ArrayList<>();

        for (Map<String, Object> gatheringEvent : gatheringEvents) {
          if (gatheringEvent != null) {
            List<HashMap<String, Object>> siteCoordinates =
                (List<HashMap<String, Object>>) gatheringEvent.get("siteCoordinates");
            siteCoordinatesUpdated = new ArrayList<>();

            if (siteCoordinates != null) {
              for (HashMap<String, Object> siteCoordinate : siteCoordinates) {
                if (siteCoordinate.get("longitudeDecimal") != null
                    && siteCoordinate.get("latitudeDecimal") != null) {
                  if (siteCoordinate.get("geoShape") == null) {

                    Double longitudeDecimal = null;
                    Double latitudeDecimal = null;
                    if (siteCoordinate.get("longitudeDecimal") instanceof Double) {
                      longitudeDecimal = (Double) siteCoordinate.get("longitudeDecimal");
                    } else if (siteCoordinate.get("longitudeDecimal") instanceof Integer) {
                      longitudeDecimal =
                          ((Integer) siteCoordinate.get("longitudeDecimal")).doubleValue();
                    }
                    if (siteCoordinate.get("latitudeDecimal") instanceof Double) {
                      latitudeDecimal = (Double) siteCoordinate.get("latitudeDecimal");
                    } else if (siteCoordinate.get("latitudeDecimal") instanceof Integer) {
                      latitudeDecimal =
                          ((Integer) siteCoordinate.get("latitudeDecimal")).doubleValue();
                    }
                    if (longitudeDecimal != null && latitudeDecimal != null) {
                      GeoShape geoShape = new GeoShape(longitudeDecimal, latitudeDecimal);
                      siteCoordinate.put("geoShape", geoShape);
                    }
                  }
                }
                siteCoordinatesUpdated.add(siteCoordinate);
              }
              if (siteCoordinatesUpdated.size() > 0) {
                gatheringEvent.put("siteCoordinates", siteCoordinatesUpdated);
              }
            }
            gatheringEventsUpdated.add(gatheringEvent);
          }
        }
        map.put("gatheringEvents", gatheringEventsUpdated);
      }

      // The document is now ready to be added to the bulk indexer
      String json = objectMapper.writeValueAsString(map);
      bulkRequest.add(new IndexRequest(index).id(id).source(json, XContentType.JSON));
//      buffer.append(getHeader(id));
//      buffer.append(json);
//      buffer.append("\n");
    }

    // Send the buffer to ES
    BulkResponse bulkResponse = client.bulk(bulkRequest, RequestOptions.DEFAULT);
    List<LoadError> loadErrors = new ArrayList<>();

    if (bulkResponse.hasFailures()) {
      // What is wrong?
      for (BulkItemResponse bulkItemResponse : bulkResponse) {
        if (bulkItemResponse.isFailed()) {
          BulkItemResponse.Failure failure = bulkItemResponse.getFailure();
          String id = failure.getId();
          String type = failure.getType();
          String reason = failure.getMessage();

          LoadError loadError = new LoadError(id, type, reason);
          loadErrors.add(loadError);
          logger.warn(loadError.toString());
        }
      }
    }

    int numberOfErrors = loadErrors.size();
    int documentsLoaded = documents.size() - parseErrors - numberOfErrors;
    job.addDocumentsProcessed(DocumentType.forIndex(index), documents.size());
    job.addDocumentsLoaded(DocumentType.forIndex(index), documentsLoaded);
    job.addLoadErrors(documentType, loadErrors);
    logger.info("{} documents were processed, with {} errors", documentsLoaded, numberOfErrors);
    ESUtil.refreshIndex(client, index);

//    Request request = new Request("POST", "/_bulk");
//    HttpEntity entity = new NStringEntity(buffer.toString(), ContentType.APPLICATION_JSON);
//    request.setEntity(entity);
//    List<LoadError> loadErrors = new ArrayList<>();
//    try {
//      Response response = client.performRequest(request);
//      entity = response.getEntity();
//      String responseBody = EntityUtils.toString(entity);
//      JsonNode jsonNode = objectMapper.readTree(responseBody);
//
//      boolean errors = jsonNode.get("errors").asBoolean();
//      if (errors) {
//        numberOfErrors += jsonNode.findValues("error").size();
//        documentsLoaded = documents.size() - numberOfErrors;
//        job.addToTotalErrors(numberOfErrors);
//
//        JsonNode items = jsonNode.get("items");
//        for (JsonNode item : items) {
//          if (item.findValue("error") != null) {
//            String id = item.findValue("_id").asText();
//            String type = item.findValue("error").get("type").asText();
//            String reason = item.findValue("error").get("reason").asText();
//            if (item.findValue("error").findValue("caused_by") != null) {
//              String causedBy = item.findValue("error").get("caused_by").get("reason").asText();
//              if (causedBy != null) reason = reason.concat(" caused by: ").concat(causedBy);
//            }
//            LoadError loadError = new LoadError(id, type, reason);
//            loadErrors.add(loadError);
//            logger.warn(loadError.toString());
//          }
//        }
//      } else {
//        documentsLoaded = documents.size();
//      }
//    } catch (SocketTimeoutException e) {
//      logger.warn("SocketTimeoutException but we'll continue ...");
//      job.addProcessError(e.getMessage());
//    } catch (ResponseException e) {
//      logger.warn("ResponseException while processing batch: " + e.getMessage());
//      if (errorFile == null) {
//        errorFile =
//            new File(
//                LoaderConfig.getInstance()
//                    .getReportsDirectory()
//                    .getAbsolutePath()
//                    .concat("/load-errors-")
//                    .concat(job.getJobId())
//                    .concat(".ndjson"));
//      }
//      logger.warn("Writing failed documents to file: {}", errorFile.getName());
//      BufferedWriter writer = new BufferedWriter(new FileWriter(errorFile, true));
//      writer.append(buffer.toString());
//      writer.close();
//      job.addProcessError(e.getMessage() + "\n" + e.getResponse());
//    } finally {
//      job.addDocumentsProcessed(DocumentType.forIndex(index), documents.size());
//      job.addDocumentsLoaded(DocumentType.forIndex(index), documentsLoaded);
//      job.addLoadErrors(documentType, loadErrors);
//      logger.info("{} documents were processed, with {} errors", documents.size(), numberOfErrors);
//      buffer.setLength(0);
//      ESUtil.refreshIndex(client, index);
//    }
  }

  private String getHeader(String id) {
    /*
     * WATCH OUT! The action needs to be "index", not "create"!
     *
     *  create : Create a document only if the document does not already exist
     *  index  : Create a new document or replace an existing document.
     *
     */
    String header = "{ \"index\" : { \"_index\" : \"%s\" , \"_id\" : \"%s\" } }\n";
    return String.format(header, index, id);
  }
}

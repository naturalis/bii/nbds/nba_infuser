package nl.naturalis.colander.infuser;

import java.time.LocalDateTime;

/**
 * Java bean for collecting Document Data.
 */
public class DocInfo {

  private String id;
  private String timeStamp;
  private String state;

  public DocInfo(String id) {
    this.id = id;
    this.setTimeStamp();
    this.state = "update";
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp() {
    this.timeStamp = LocalDateTime.now().toString();
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }
  
}

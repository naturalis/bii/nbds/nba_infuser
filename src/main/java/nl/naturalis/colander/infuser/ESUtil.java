package nl.naturalis.colander.infuser;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.naturalis.colander.infuser.model.Alias;
import nl.naturalis.colander.infuser.model.DocumentType;
import nl.naturalis.colander.infuser.model.ImportMetadata;
import nl.naturalis.colander.infuser.model.Job;
import nl.naturalis.colander.infuser.model.SourceSystem;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest.AliasActions;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest.AliasActions.Type;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.flush.FlushRequest;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.GetAliasesResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.cluster.health.ClusterHealthStatus;
import org.elasticsearch.cluster.metadata.AliasMetadata;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Utility class for interacting with Elasticsearch.
 *
 */
@SuppressWarnings("CheckStyle")
class ESUtil {

  private static final Logger logger = LogManager.getLogger(ESUtil.class);
  private static final ObjectMapper mapper = new ObjectMapper();
  private static final ClassLoader classLoader = ESUtil.class.getClassLoader();
  private static final Job job = Job.getInstance();
  private static final String META_INDEX = "meta";
  private static final String METADATA_DOC_ID = "importdata";

  /**
   * Get the http status of the ES cluster.
   *
   * @return String status
   * @throws IOException when the status request fails
   */
  public String getStatus(RestHighLevelClient client) throws IOException {
    GetAliasesRequest request = new GetAliasesRequest();
    GetAliasesResponse response = client.indices().getAlias(request, RequestOptions.DEFAULT);
    RestStatus status = response.status();
    return String.format("%s (%d)", status, status.getStatus());
  }

  /**
   * Verify if there's an index with the name given.
   *
   * @param index  the name of the index
   * @return true if the index is available; false otherwise
   * @throws IOException when the index request fails
   */
  static boolean indexExists(RestHighLevelClient client, String index) throws IOException {
    GetIndexRequest request = new GetIndexRequest(index);
    return client.indices().exists(request, RequestOptions.DEFAULT);
  }

  /**
   * List all index names.
   *
   * @return a Set containing all index names
   * @throws IOException when the alias request fails
   */
  static Set<String> listIndices(RestHighLevelClient client) throws IOException {

    GetAliasesRequest request = new GetAliasesRequest();
    GetAliasesResponse response = client.indices().getAlias(request, RequestOptions.DEFAULT);
    Map<String, Set<AliasMetadata>> aliases = response.getAliases();
    if (aliases == null) {
      return null;
    }
    return aliases.keySet();
  }

  /**
   * List all indices belonging to the specified document type.
   *
   * @return a Set containing all index names
   * @throws IOException when the alias request fails
   */
  static Set<String> listIndices(RestHighLevelClient client, DocumentType documentType) throws IOException {

    Set<String> allIndices = listIndices(client);
    if (allIndices == null) {
      return null;
    }
    Set<String> indices = new HashSet<>();
    for (String index : allIndices) {
      if (index.contains(documentType.fullName().toLowerCase())) {
        indices.add(index);
      }
    }
    if (indices.isEmpty()) {
      return null;
    }
    return indices;
  }

  /**
   * Returns a {@link Set} of all indexes that have the alias name provided.
   *
   * @param client : the client
   * @param alias : the Alias for which you want to find out which indexes use it
   * @return a Set containing all index names
   * @throws IOException when the alias request fails
   */
  static Set<String> listIndices(RestHighLevelClient client, Alias alias)
      throws IOException {

    Set<String> indices = new HashSet<>();
    Set<String> allIndices = listIndices(client);
    if (allIndices == null || allIndices.isEmpty()) {
      return null;
    }
    for (String index : allIndices) {
      if (index.contains(alias.getAliasName().toLowerCase())) {
        indices.add(index);
      }
    }
    if (indices.isEmpty()) {
      return null;
    }
    return indices;
  }

  /*
   * Get the active index for the specified document type and data supplier.
   *
   */
  static String getActiveIndex(
      RestHighLevelClient client, DocumentType documentType, String dataSupplier)
      throws IOException {

    List<String> indices = new ArrayList<>();

    Alias alias = documentType.getAlias();
    Set<String> candidates = listIndices(client, alias);
    if (candidates == null) {
      return null;
    }
    for (String index : candidates) {
      if (index.contains(dataSupplier.toLowerCase())) {
        indices.add(index);
      }
    }
    if (indices.isEmpty()) {
      logger.info(
          "No index exists for documents from data supplier \"{}\" with alias \"{}\"",
          dataSupplier,
          alias.getAliasName());
      return null;
    } else if (indices.size() > 1) {
      String msg = String.format(
              "There is more than one index from data supplier %s with alias \"%s\"",
              dataSupplier, alias.getAliasName());
      logger.warn(msg);
      indices.sort(Collections.reverseOrder());
      logger.warn("Only the most recent is used as backup index: {}", indices.get(0));
      return indices.get(0);
    }
    return indices.get(0);
  }

  /*
   * Create an index with a predefined mapping.
   *
   * NOTE: index may not already exist!
   */
  static boolean createIndex(RestHighLevelClient client, String index) throws IOException {

    if (indexExists(client, index)) {
      logger.info("Index \"{}\" already exists", index);
      return false;
    }
    DocumentType documentType;
    try {
      documentType = DocumentType.forIndex(index);
    } catch (IOException e) {
      logger.error("Unrecognised index name: \"{}\". Could not create index. Error: {}", index, e.getMessage());
      return false;
    }

    ClassLoader classLoader = ESUtil.class.getClassLoader();

    // Settings
    InputStream inputStream = classLoader.getResourceAsStream("es-settings.json");
    if (inputStream == null) {
      String msg = "Elasticsearch settings config-file is missing: es-settings.json";
      logger.error(msg);
      throw new RuntimeException(msg);
    }
    String settings = IOUtils.toString(inputStream, StandardCharsets.UTF_8);

    // Mapping
    inputStream = classLoader.getResourceAsStream("es-mapping-" + documentType.name().toLowerCase() + ".json");
    if (inputStream == null) {
      String msg = String.format("Mapping for index %s is missing", index);
      logger.error(msg);
      throw new RuntimeException(msg);
    }
    String mapping = IOUtils.toString(inputStream, StandardCharsets.UTF_8);

    CreateIndexRequest request = new CreateIndexRequest(index);
    request.settings(settings, XContentType.JSON);
    request.mapping(mapping, XContentType.JSON);
    boolean acknowledged = false;
    try {
      CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
      acknowledged = createIndexResponse.isAcknowledged();
    } catch (ElasticsearchStatusException e) {
      logger.error("Failed to create index: {}", e.getMessage());
      // TODO: in case of failure, the job file should be suffixed with "error" (or something)
    }
    if (acknowledged) {
      logger.info("Created index: {}", index);
    } else {
      logger.warn("Failed to create index: {}", index);
    }
    return acknowledged;
  }

  /*
   * Delete an index
   */
  static boolean deleteIndex(RestHighLevelClient client, String index) {

    if (index == null) {
      throw new IllegalArgumentException("Missing index name");
    }

    try {
      DeleteIndexRequest request = new DeleteIndexRequest(index);
      request.timeout("2m");
      AcknowledgedResponse deleteIndexResponse = client.indices().delete(request, RequestOptions.DEFAULT);
      boolean acknowledged = deleteIndexResponse.isAcknowledged();
      if (acknowledged) {
        logger.info("Deleted index: {}", index);
      } else {
        logger.error("Error deleting index: {}", index);
      }
      return acknowledged;
    } catch (ElasticsearchException e) {
      if (e.status() == RestStatus.NOT_FOUND) {
        logger.warn("There is no index: {}", index);
      }
    } catch (IOException e) {
      logger.error("Failed to delete index: {}", index);
    }
    return false;
  }

  /*
   * Delete ALL indexes!!!
   */
  static boolean deleteAll(RestHighLevelClient client) throws IOException {

    boolean success = true;
    Set<String> indices = listIndices(client);
    if (indices == null) {
      return true;
    }
    for (String index : indices) {
      if (!deleteIndex(client, index)) {
        success = false;
      }
    }
    return success;
  }

  /*
   * Return a Set of all aliases of a specific index
   */
  static Set<String> listAliases(RestHighLevelClient client, String index) throws IOException {

    Set<String> aliases = new HashSet<>();
    if (!indexExists(client, index)) {
      return null;
    }
    GetAliasesRequest request = new GetAliasesRequest();
    request.indices(index);
    GetAliasesResponse response = client.indices().getAlias(request, RequestOptions.DEFAULT);
    Map<String, Set<AliasMetadata>> map = response.getAliases();
    if (map != null) {
      for (AliasMetadata alias : map.get(index)) {
        aliases.add(alias.getAlias());
      }
    }
    return aliases;
  }

  /*
   * Check if an alias name has been assigned to a specific index
   */
  static boolean indexHasAlias(RestHighLevelClient client, String index, Alias alias) throws IOException {
    Set<String> aliases = listAliases(client, index);
    if (aliases == null || aliases.isEmpty()) {
      return false;
    }
    return aliases.contains(alias.getAliasName());
  }

  /*
   * Assign an alias to a specific index
   */
  static boolean addAliasToIndex(RestHighLevelClient client, String index, Alias alias) throws IOException {
    IndicesAliasesRequest request = new IndicesAliasesRequest();
    AliasActions aliasAction =
        new AliasActions(AliasActions.Type.ADD)
            .index(index)
            .alias(alias.getAliasName());
    request.addAliasAction(aliasAction);
    AcknowledgedResponse indicesAliasesResponse =
        client.indices().updateAliases(request, RequestOptions.DEFAULT);
    boolean acknowledged = indicesAliasesResponse.isAcknowledged();
    if (!acknowledged) {
      logger.error("Failed to alias the index {} with alias name: {}", index, alias.getAliasName());
    }
    return acknowledged;
  }

  /*
   * Remove all aliases from an index
   */
  static boolean removeAliasFromIndex(RestHighLevelClient client, String index, Alias alias) throws IOException {
    IndicesAliasesRequest request = new IndicesAliasesRequest();
    AliasActions aliasAction =
        new AliasActions(Type.REMOVE)
            .index(index)
            .alias(alias.getAliasName());
    request.addAliasAction(aliasAction);
    AcknowledgedResponse indicesAliasesResponse =
        client.indices().updateAliases(request, RequestOptions.DEFAULT);
    return indicesAliasesResponse.isAcknowledged();
  }


  /*
   * Remove all aliases from an index
   */
  static boolean removeAliasesFromIndex(RestHighLevelClient client, String index) throws IOException {
    Set<String> aliases = listAliases(client, index);
    if (aliases == null || aliases.isEmpty()) {
      logger.info("Noting to do. Index has no alias(es).");
      return true;
    }
    boolean success = true;
    for (String alias : aliases) {
      IndicesAliasesRequest request = new IndicesAliasesRequest();
      AliasActions aliasAction =
          new AliasActions(Type.REMOVE)
              .index(index)
              .alias(alias);
      request.addAliasAction(aliasAction);
      AcknowledgedResponse indicesAliasesResponse =
          client.indices().updateAliases(request, RequestOptions.DEFAULT);
      if (!indicesAliasesResponse.isAcknowledged()) {
        success = false;
        logger.error("Removing alias {} from index {} failed.", alias, index);
      }
    }
    return success;
  }

  /*
   * Removes an alias from all(!) indexes
   */
  static boolean removeAlias(RestHighLevelClient client, Alias alias) throws IOException {

    boolean success = false;
    Set<String> allIndexes = listIndices(client);
    if (allIndexes == null) {
      logger.info("There are no indexes. No alias was removed.");
      return true;
    }
    List<String> matchingIndexes = new ArrayList<>();
    for (String index : allIndexes) {
      if (index.contains(alias.getAliasName())) {
        matchingIndexes.add(index);
      }
    }
    if (!matchingIndexes.isEmpty()) {
      for (String index : matchingIndexes) {
        success = removeAliasesFromIndex(client, index);
      }
    } else {
      logger.info("There is no index with alias {}", alias);
    }
    logger.info("Alias {} has been removed from all indexes", alias);
    return success;
  }

  static Alias getAlias(String index) {
    if (index == null) {
      return null;
    }
    for (Alias alias : Alias.values()) {
      if (index.contains(alias.getAliasName())) {
        return alias;
      }
    }
    return null;
  }

  static boolean hasAlias(RestHighLevelClient client, String index) throws IOException {
    GetAliasesRequest request = new GetAliasesRequest();
    request.indices(index);
    GetAliasesResponse response = client.indices().getAlias(request, RequestOptions.DEFAULT);
    Map<String, Set<AliasMetadata>> aliases = response.getAliases();
    return (aliases != null && !aliases.isEmpty());
  }

//  /*
//   * Utility method for creating an IndexMap
//   */
//  @Deprecated
//  static IndexMap getIndexMap(RestHighLevelClient client) throws IOException {
//
//    Request request = new Request(GET, "/_aliases");
//    Response response;
//    try {
//      response = client.performRequest(request);
//      String responseBody = EntityUtils.toString(response.getEntity());
//      mapper = new ObjectMapper();
//      Map<String, Map<String, Map<String, Object>>> map;
//      map =
//          mapper.readValue(
//              responseBody, new TypeReference<Map<String, Map<String, Map<String, Object>>>>() {});
//      return new IndexMap(map);
//    } catch (ResponseException e) {
//      return null;
//    }
//  }

  /*
   * Refresh a specific index
   */
  static boolean refreshIndex(RestHighLevelClient client, String index) throws IOException {
    if (index == null) {
      throw new IllegalArgumentException("Cannot refresh index! Index name is missing");
    }
    try {
      RefreshRequest request = new RefreshRequest(index);
      client.indices().refresh(request, RequestOptions.DEFAULT);
      logger.debug("Index {} has been refreshed", index);
      return true;
    } catch (ElasticsearchException exception) {
      if (exception.status() == RestStatus.NOT_FOUND) {
        logger.warn("There is no index {}. Nothing to refresh.", index);
      }
      return false;
    }
  }

  /*
   * Flush a specific index
   */
  static boolean flushIndex(RestHighLevelClient client, String index) throws IOException {
    if (index == null) {
      throw new IllegalArgumentException("Cannot flush index! Index name is missing");
    }
    FlushRequest request = new FlushRequest(index);
    try {
      client.indices().flush(request, RequestOptions.DEFAULT);
      logger.debug("Index {} has been flushed", index);
      return true;
    } catch (ElasticsearchException exception) {
      if (exception.status() == RestStatus.NOT_FOUND) {
        logger.warn("There is no index {}. Nothing to flush.", index);
      }
      return false;
    }
  }

  /*
   * Flush all indexes
   */
  static boolean flushAll(RestHighLevelClient client) throws IOException {
    FlushRequest requestAll = new FlushRequest();
    try {
      client.indices().flush(requestAll, RequestOptions.DEFAULT);
      logger.info("All indices flushed");
      return true;
    } catch (ElasticsearchException exception) {
      if (exception.status() == RestStatus.NOT_FOUND) {
        logger.warn("Failed to flush all indices");
      }
      return false;
    }
  }

  /**
   * Count the total number of documents in a specific index.
   *
   * @param client : client
   * @param index : index
   * @return : returns the total number of documents for the index name given
   * @throws IOException : errors when there is something wrong with the connection
   */
  public static long countDocuments(RestHighLevelClient client, String index) throws IOException {
    refreshIndex(client, index);
    CountRequest countRequest = new CountRequest(index);
    countRequest.query(QueryBuilders.matchAllQuery());
    CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
    long count = countResponse.getCount();
    RestStatus status = countResponse.status();
    Boolean terminatedEarly = countResponse.isTerminatedEarly();
    return count;
  }

  static String clusterHealth(RestHighLevelClient client) {

    ClusterHealthRequest request = new ClusterHealthRequest();
    ClusterHealthResponse response;
    try {
      response = client.cluster().health(request, RequestOptions.DEFAULT);
      ClusterHealthStatus status = response.getStatus();
      boolean timedOut = response.isTimedOut();
      return status.name();
    } catch (IOException e) {
        logger.error("Failed to get cluster health: {}", e.getMessage());
      return null;
    }
  }

  static SourceSystem getSourceSystem(String index) {
    if (index == null) {
      return null;
    }
    String[] parts = index.split("-", 3);
    String sourceStr = parts[1];
    for (SourceSystem sourceSystem : SourceSystem.values()) {
      if (sourceStr.toUpperCase().equals(sourceSystem.toString())) {
        return sourceSystem;
      }
    }
    return null;
  }

  @Deprecated
  private static String getActionString(String type, String index, String alias) {
    if (type == null
        || index == null
        || alias == null
        || !(type.equals("add") || type.equals("remove"))) {
      throw new IllegalArgumentException();
    }
    String actionStr =
        "{"
            + "\"actions\" : ["
            + "{ \"%s\" : { \"index\" : \"%s\", \"alias\" : \"%s\" } }"
            + "]"
            + "}";
    return String.format(actionStr, type, index, alias);
  }

  static void updateMetadataIndex(RestHighLevelClient client, DocumentType documentType) throws IOException {

    IndexRequest request;
    boolean updateMetadata = true;

    // Check for the metadata index. Recreate if necessary
    if (!indexExists(client, META_INDEX)) {
      updateMetadata = createMetaIndex(client);
    }
    if (!updateMetadata) {
      logger.info("Not updating the metadata index");
      return;
    }

    // Collect the current metadata
    Map<String, Object> sourceAsMap;
    GetRequest getRequest = new GetRequest(META_INDEX, METADATA_DOC_ID);
    GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);
    if (getResponse.isExists()) {
      sourceAsMap = getResponse.getSourceAsMap();
    } else {
      logger.error("Updating metadata document failed!");
      return;
    }

    // Update the document with the current import
    String key = job.getDataSupplier()
                    .toLowerCase()
                    .concat("_")
                    .concat(documentType.fullName().toLowerCase());
    ImportMetadata importData = new ImportMetadata(job, documentType);
    sourceAsMap.put(key, importData.getSourceAsMap());

    // Put the updated document back in the index
    request = new IndexRequest(META_INDEX);
    request.id(METADATA_DOC_ID);
    request.source(sourceAsMap);

    logger.info(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(sourceAsMap));
    RestStatus status = null;
    try {
      IndexResponse response = client.index(request, RequestOptions.DEFAULT);
      status = response.status();
    } catch (ElasticsearchException e) {
      logger.error("Error while creating the meta index: {}", e.getMessage());
    }
    if (status == RestStatus.OK) {
      logger.info("Updated \"meta\" index");
    } else {
      String msg = "Failed to update the metadata index";
      logger.warn(msg);
      job.addComment(msg);
    }
  }

  public static boolean createMetaIndex(RestHighLevelClient client) {

    // read settings and mapping
    String settings = readConfiguration("es-settings.json");
    String mapping = readConfiguration("es-mapping-meta.json");

    // create index
    CreateIndexRequest indexRequest = new CreateIndexRequest(META_INDEX);
    indexRequest.settings(settings, XContentType.JSON);
    indexRequest.mapping(mapping, XContentType.JSON);
    try {
      CreateIndexResponse createIndexResponse = client.indices().create(indexRequest, RequestOptions.DEFAULT);
      boolean acknowledged = createIndexResponse.isAcknowledged();
      logger.info("Successfully created meta index");
      return true;
    } catch (ElasticsearchException | IOException e) {
      logger.error("Failed to create meta index: {}", e.getMessage());
      return false;
    }
  }

  private static String readConfiguration(String configFile) {
      try (InputStream inputStream = classLoader.getResourceAsStream(configFile)) {
          if (inputStream == null) {
            String msg = "Configuration file for the meta index is missing: " + configFile;
            logger.error(msg);
            throw new RuntimeException(msg);
          }
          else {
            return IOUtils.toString(inputStream, StandardCharsets.UTF_8);
          }
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
  }

  public static String prettyPrintJsonNode(String jsonStr) {
    try {
      Object json = mapper.readValue(jsonStr, Object.class);
      return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
    } catch (IOException e) {
      System.out.println("Failed to pretty print JsonNode object ...");
      return "";
    }
  }
}

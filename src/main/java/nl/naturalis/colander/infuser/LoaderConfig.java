package nl.naturalis.colander.infuser;

import java.io.File;
import java.io.IOException;
import nl.naturalis.colander.infuser.model.Job;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoaderConfig {

  private static final Job job = Job.getInstance();

  private String host;
  private int port;
  private String scheme;
  private int batchSize = 1000;

  private boolean addGeoShape = true;
  private boolean cleanUp = true;
  
  private File jobsDirectory;
  private File incomingDirectory;
  private File archiveDirectory;
  private File reportsDirectory;
  
  private String textMessageHost;
  private boolean sendTextMessages = false;
    
  private static LoaderConfig instance;
  
  private static final Logger logger = LogManager.getLogger(LoaderConfig.class);

  static LoaderConfig getInstance() throws IOException {
    if (instance == null) {
      instance = new LoaderConfig();
    }
    return instance;
  }
  
  private LoaderConfig() throws IOException {
    loadProperties();
  }
  
  String getHost() {
    return host;
  }

  int getPort() {
    return port;
  }

  String getScheme() {
    return scheme;
  }
  
  int getBatchSize() {
    return batchSize;
  }

  public boolean cleanUp() {
    return cleanUp;
  }

  boolean addGeoShape() {
    return addGeoShape;
  }

  File getJobsDirectory() {
    return jobsDirectory;
  }
  
  File getIncomingDirectory() {
    return incomingDirectory;
  }

  File getArchiveDirectory() {
    return archiveDirectory;
  }

  File getReportsDirectory() {
    return reportsDirectory;
  }

  String getTextMessageHost() {
    return textMessageHost;
  }

  public void setTextMessageHost(String textMessageHost) {
    this.textMessageHost = textMessageHost;
  }

  boolean sendTextMessages() {
    return sendTextMessages;
  }

  /**
   * Send text messages or not
   *
   * @param sendTextMessages true for sending text messages; false otherwise.
   */
  public void sendTextMessages(boolean sendTextMessages) {
    this.sendTextMessages = sendTextMessages;
  }

  private void loadProperties() throws IOException {
    
    // NBA_HOST
    host = System.getenv("NBA_HOST");
    if (host == null || host.trim().isEmpty()) {
      throw new IOException("Missing property: NBA_HOST");
    }
    host = host.trim();
    
    // NBA_PORT
    String portStr = System.getenv("NBA_PORT");
    if (portStr == null || portStr.trim().isEmpty()) {
      throw new IOException("Missing property: NBA_PORT");
    }
    try {
      port = Integer.parseInt(portStr.trim());
    } catch (NumberFormatException e) {
      throw new IOException(
          String.format("Invalid value for property: NBA_PORT : %s", System.getenv("NBA_PORT")));
    }
    
    // NBA_SCHEME
    scheme = System.getenv("NBA_SCHEME");
    if (scheme == null || scheme.trim().isEmpty()) {
      throw new IOException("Missing property: NBA_SCHEME");
    }
    scheme = scheme.trim();
    
    // batch size
    String batchSizeStr = System.getenv("INFUSER_BATCHSIZE");
    if (batchSizeStr == null || batchSizeStr.trim().isEmpty()) {
      logger.info("Batch size has not been set. Using default value: 1000");
      batchSize = 1000;
    } else {
      try {
        batchSize = Integer.parseInt(batchSizeStr.trim());        
      } catch (NumberFormatException e) {
        throw new IOException(
            String.format(
                "Invalid value for property: INFUSER_BATCHSIZE : %s",
                System.getenv("INFUSER_BATCHSIZE")));
      }
    }

    // cleanUp
    cleanUp = Boolean.parseBoolean(System.getenv("INFUSER_CLEANUP"));
    job.cleanUp(cleanUp);
    if (cleanUp) {
      logger.info("Clean up: try deleting overdue indexes.");
    }

    // add geoShape
    addGeoShape = Boolean.parseBoolean(System.getenv("INFUSER_ADD_GEOSHAPE"));
    if (addGeoShape) {
      job.addGeoShape(true);
      logger.info("geoShape will be added to gatheringSite(s) when possible");
    }

    // job files
    String jobsPathStr = System.getenv("INFUSER_JOBSPATH");
    if (jobsPathStr == null || jobsPathStr.trim().isEmpty()) {
      throw new IOException("Missing property: INFUSER_JOBSPATH");
    }
    jobsDirectory = verifyDir(System.getenv("INFUSER_JOBSPATH").trim());

    // json documenten (update files)
    String incomingPathStr = System.getenv("INFUSER_INCOMINGPATH");
    if (incomingPathStr == null || incomingPathStr.trim().isEmpty()) {
      throw new IOException("Missing property: INFUSER_INCOMINGPATH");
    }
    incomingDirectory = verifyDir(System.getenv("INFUSER_INCOMINGPATH").trim());
    
    // archive
    String archivePathStr = System.getenv("INFUSER_ARCHIVEPATH"); 
    if (archivePathStr == null || archivePathStr.trim().isEmpty()) {
      throw new IOException("Missing property: INFUSER_ARCHIVEPATH");
    }
    archiveDirectory = verifyDir(System.getenv("INFUSER_ARCHIVEPATH").trim());

    // reports
    String reportsPathStr = archiveDirectory.getParent().concat("/reports");
    reportsDirectory = verifyDir(reportsPathStr);

    // Text Message Webhook
    String sendTextMessageStr = System.getenv("TEXT_MESSAGING_ENABLED");
    if (sendTextMessageStr != null && !sendTextMessageStr.trim().isEmpty()) {
      sendTextMessages = sendTextMessageStr.trim().equals("1")
          || sendTextMessageStr.trim().equalsIgnoreCase("true");
    }
    textMessageHost = System.getenv("TEXT_MESSAGE_WEBHOOK");
    if (textMessageHost == null || textMessageHost.trim().isEmpty()) {
      sendTextMessages = false;
    } else {
      textMessageHost = textMessageHost.trim();
    }
    if (sendTextMessages) {
      logger.info("Text messaging is enabled");
    }
  }
  
  /**
   * Verifies if the directory exists and creates one if it doesn't.
   *
   * @param dirName the directory name to be checked or created
   * @return returns the directory for the directory name given
   * @throws IOException throws an IOException when the directory could not be created
   */
  private File verifyDir(String dirName) throws IOException {
    if (dirName == null || dirName.isEmpty()) {
      throw new InitializationException("Directory name may not be null!");
    }
    File directory = new File(dirName);
    if (!directory.exists()) {
      logger.info("Creating: {}", dirName);
      boolean created = new File(dirName).mkdir();
      if (!created) {
        throw new IOException(String.format("Unable to create directory: %s", dirName));
      }
    }
    return directory;
  }

}

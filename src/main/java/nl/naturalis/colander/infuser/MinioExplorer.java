package nl.naturalis.colander.infuser;

import io.minio.MinioClient;
import io.minio.Result;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidArgumentException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.MinioException;
import io.minio.errors.NoResponseException;
import io.minio.messages.Item;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xmlpull.v1.XmlPullParserException;

/**
 * MinioExplorer.
 *
 * @author Tom Gilissen
 */
@Deprecated
public class MinioExplorer {

  private static final String SYSPROP_CONF_FILE = "minio.conf.file";
  private static final Logger logger = LogManager.getLogger(MinioExplorer.class);
  private final File cfgFile;
  private final String ACCESS_KEY;
  private final String SECRET_KEY;
  private final LoaderConfig config;
  private final String HOST;
  private final String BUCKET;
  private final String JOBS_BUCKET;
  private final File jobsDirectory;
  private MinioClient minioClient;

  @Deprecated
  public MinioExplorer() throws InvalidEndpointException, InvalidPortException, IOException {
    config = LoaderConfig.getInstance();
    this.jobsDirectory = config.getJobsDirectory();
    this.HOST = ""; // config.getMinioHost();
    this.BUCKET = ""; // config.getIncrementalBucket();
    this.JOBS_BUCKET = ""; // config.getJobsBucket();
    String path = System.getProperty(SYSPROP_CONF_FILE);
    if (path == null) {
      String msg = String.format("Missing system property: %s", SYSPROP_CONF_FILE);
      throw new InitializationException(msg);
    }
    cfgFile = new File(path);
    if (!cfgFile.isFile()) {
      String msg = String.format("Missing minio server credentials file: %s", cfgFile.getName());
      throw new InitializationException(msg);
    }
    logger.info("Minio server credentials file: {}", cfgFile.getName());

    Properties properties = new Properties();
    try (InputStream input = new FileInputStream(cfgFile)) {
      properties.load(input);
      ACCESS_KEY = properties.getProperty("access_key");
      SECRET_KEY = properties.getProperty("secret_key");
    } catch (IOException e) {
      String msg =
          String.format(
              "Could not read configuration file: %s : %s", cfgFile.getPath(), e.getMessage());
      throw new InitializationException(msg);
    }
    minioClient = new MinioClient(HOST, ACCESS_KEY, SECRET_KEY);
    logger.info("Minio server connection is ready");
  }

  public List<String> listUpdateFiles()
      throws InvalidKeyException, InvalidBucketNameException, NoSuchAlgorithmException,
          InsufficientDataException, NoResponseException, ErrorResponseException, InternalException,
          IOException, XmlPullParserException {

    List<String> files = new ArrayList<>();
    boolean found = false;
    found = minioClient.bucketExists(BUCKET);
    if (found) {
      Iterable<Result<Item>> fileObjects;
      fileObjects = minioClient.listObjects(BUCKET);
      for (Result<Item> fileObject : fileObjects) {
        Item item = fileObject.get();
        files.add(item.objectName());
      }
      return files;
    }
    return null;
  }

  public List<String> listJobFiles()
      throws InvalidKeyException, InvalidBucketNameException, NoSuchAlgorithmException,
          InsufficientDataException, NoResponseException, ErrorResponseException, InternalException,
          IOException, XmlPullParserException {

    List<String> jobFiles = new ArrayList<>();
    boolean found = false;
    found = minioClient.bucketExists(JOBS_BUCKET);
    if (found) {
      Iterable<Result<Item>> fileObjects;
      fileObjects = minioClient.listObjects(JOBS_BUCKET, "jobs/");
      for (Result<Item> fileObject : fileObjects) {
        Item item = fileObject.get();
        Pattern jsonFile = Pattern.compile(".*[.]json$");
        if (!item.isDir() && !item.isEmpty() && jsonFile.matcher(item.objectName()).matches()) {
          jobFiles.add(item.objectName());
        }
      }
      return jobFiles;
    }
    return null;
  }

  public boolean downloadFile(String fileName) {

    String targetFile = jobsDirectory.getName().concat("/" + fileName);
    try (InputStream input = minioClient.getObject(BUCKET, fileName);
        OutputStream output = new FileOutputStream(targetFile)) {
      // Check whether the object exists using statObject().
      // If the object is not found statObject() throws an exception,
      // else it means that the object exists.
      minioClient.statObject(BUCKET, fileName);

      // Read the input stream and print to the console till EOF.
      byte[] buf = new byte[16384];
      int bytesRead;
      while ((bytesRead = input.read(buf, 0, buf.length)) >= 0) {
        output.write(buf, 0, bytesRead);
      }
      return true;
    } catch (InvalidBucketNameException e) {
      logger.error(
          "The minio server doesn't contain a bucket with name {}. Downloading of file {} failed: {}",
          BUCKET,
          fileName,
          e);
      return false;
    } catch (MinioException
        | IOException
        | InvalidKeyException
        | NoSuchAlgorithmException
        | XmlPullParserException e) {
      logger.error("File {} was not downloaded because an error occurred: {}", fileName, e);
      return false;
    }
  }

  public void moveToDownloaded(String fileName) {
    String destBucket = ""; // config.getFinishedBucket();
    logger.info("Moving file {} to the import directory", fileName);
    try {
      minioClient.statObject(BUCKET, fileName);
      minioClient.copyObject(BUCKET, fileName, destBucket);
      minioClient.statObject(destBucket, fileName);
      minioClient.removeObject(BUCKET, fileName);
    } catch (InvalidKeyException
        | InvalidBucketNameException
        | NoSuchAlgorithmException
        | InsufficientDataException
        | NoResponseException
        | ErrorResponseException
        | InternalException
        | IOException
        | XmlPullParserException
        | InvalidArgumentException e) {
      logger.error("Failed to move file {} to the import directory: {}", fileName, e.getMessage());
    }
  }
}

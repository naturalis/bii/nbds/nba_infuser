package nl.naturalis.colander.infuser.model;

import com.fasterxml.jackson.annotation.JsonValue;
import java.io.IOException;

/**
 * The document types present in the document store. Not that there may be multiple indexes
 * per document type. An alias added to the index will determine whether the index will
 * be included in the search or not.
 * 
 */
public enum DocumentType {

  GEOAREA("GeoArea"),
  MULTIMEDIAOBJECT("MultiMediaObject"),
  SPECIMEN ("Specimen"),
  TAXON ("Taxon");
  
  private final String fullName;

  DocumentType(String fullName) {
    this.fullName = fullName;
  }

  public String fullName() {
    return this.fullName;
  }

  @JsonValue
  public String toString() {
    return this.fullName();
  }

  /**
   * Returns the DocumentType that matches the given Index name.
   *
   * @param indexName : the index name of a specific index
   * @return DocumentType : the DocumentType that matches the given index name
   */
  public static DocumentType forIndex(String indexName) throws IOException {
    if (indexName.contains("specimen")) {
      return SPECIMEN;
    }
    if (indexName.contains("taxon")) {
      return TAXON;
    }
    if (indexName.contains("multimedia")) {
      return MULTIMEDIAOBJECT;
    }
    if (indexName.contains("geo")) {
      return GEOAREA;  // TODO: this should be geoarea ...
    }
    throw new IOException(String.format("No DocumentType for this index: %s", indexName));
  }

  /**
   * Return the Alias that corresponds to the DocumentType.
   *
   * @return the Alias that matches the DocumentType
   */
  public Alias getAlias() {
    if (this.equals(GEOAREA)) {
      return Alias.GEOAREAS;
    } else if (this.equals(MULTIMEDIAOBJECT)) {
      return Alias.MULTIMEDIA;
    } else if (this.equals(SPECIMEN)) {
      return Alias.SPECIMEN;
    } else {
      return Alias.TAXON;
    }
  }

  public boolean isGeoArea() {
    return this.equals(DocumentType.GEOAREA);
  }

  public boolean isMultiMediaObject() {
    return this.equals(DocumentType.MULTIMEDIAOBJECT);
  }

  public boolean isSpecimen() {
    return this.equals(DocumentType.SPECIMEN);
  }

  public boolean isTaxon() {
    return this.equals(DocumentType.TAXON);
  }


}


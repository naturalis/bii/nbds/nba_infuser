package nl.naturalis.colander.infuser.model;

import com.fasterxml.jackson.annotation.JsonValue;
import org.geojson.Point;

public class GeoShape {

  private Point point;

  public GeoShape(Double longitudeDecimal, Double latitudeDecimal) {
    if (longitudeDecimal != null && latitudeDecimal != null) {
      this.point = new Point(longitudeDecimal, latitudeDecimal);
    }
  }

  @JsonValue
  public Point getGeoShape() {
    return point;
  }
}

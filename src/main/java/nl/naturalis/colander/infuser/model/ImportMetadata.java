package nl.naturalis.colander.infuser.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Class modeling the import metadata present at the endpoint
 * /meta/importdata/importdata.
 */
@JsonPropertyOrder({"jobId", "index", "import_date", "harvest_date"})
public class ImportMetadata {

  private final String jobId;
  private final String index;
  private final OffsetDateTime importDate;
  private final String harvestDateStr;
  private final String etlLog;

  @JsonCreator
  public ImportMetadata(Job job, DocumentType documentType) {
    jobId = job.getJobId();
    index = job.getIndexCreated().get(documentType).getIndexName();
    importDate = job.getImportDate();
    harvestDateStr = parseHarvestDateStr(job, documentType);
    etlLog = job.getEtlLogFile(documentType);
  }

  private static String parseHarvestDateStr(Job job, DocumentType documentType) {
    String dateStr = job.getHarvestDateStr(documentType);
    if (dateStr != null) {
      return dateStr;
    }
    return ("unknown");
  }

  @JsonGetter("jobId")
  public String getJobId() {
    return jobId;
  }

  @JsonGetter("index")
  public String getIndex() {
    return index;
  }

  @JsonProperty("import_date")
  public String getImportDate() {
    return importDate.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
  }

  @JsonProperty("harvest_date")
  public String getHarvestDateStr() {
    return harvestDateStr;
  }

  @JsonProperty("etl_log_file")
  public String getEtlLog() {
    return etlLog;
  }

  /**
   * Provides the metadata about an import as a {@link HashMap}.
   *
   * @return map containing the import metadata
   */
  public Map<String, String> getSourceAsMap() {
    Map<String, String> source = new HashMap<>(5);
    source.put("jobId", this.jobId);
    source.put("index", this.index);
    source.put("import_date", this.getImportDate());
    source.put("harvest_date", this.harvestDateStr);
    source.put("etl_log_file", this.etlLog);
    return source;
  }
}

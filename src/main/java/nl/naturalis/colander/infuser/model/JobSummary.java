package nl.naturalis.colander.infuser.model;

import static nl.naturalis.colander.infuser.model.DocumentType.GEOAREA;
import static nl.naturalis.colander.infuser.model.DocumentType.MULTIMEDIAOBJECT;
import static nl.naturalis.colander.infuser.model.DocumentType.SPECIMEN;
import static nl.naturalis.colander.infuser.model.DocumentType.TAXON;

public class JobSummary {

  private final Job job;

  public JobSummary(Job job) {
    this.job = job;
  }

  @Override
  public String toString() {

    if (job == null) {
      return "No job data";
    }

    if (job.getStatus() == Status.LOADED || job.getStatus() == Status.ERRORS) {

      int todo = (job.getJobsTodo() > 1) ? job.getJobsTodo() - 1 : 0;
      String message =
          "completed job *`%s`* for *%s* with status *%s* (took %s):  \n"
              + "_infusion overview:_  \n"
              + getInfoStr(SPECIMEN)
              + getInfoStr(MULTIMEDIAOBJECT)
              + getInfoStr(TAXON)
              + getInfoStr(GEOAREA)
              + getProcessErrors()
              + "Jobs left to do: %d";

      return String.format(
          message, job.getJobId(), job.getDataSupplier(), job.getStatus(), job.getDuration(), todo);
    } else if (job.getStatus() == Status.FAILURE) {
      return String.format("loading *failed* after %s :cow2:", job.getDuration());
    } else {
      // job.getStatus() == Status.IDLE
      return "";
    }
  }

  private String getInfoStr(DocumentType documentType) {

    String infoStr = "";
    String indexCreatedInfo = "";

    Index indexCreated =
        (job.getIndexCreated() != null) ? job.getIndexCreated().get(documentType) : null;
    if (indexCreated != null) {
      String indexCreatedName = indexCreated.getIndexName();
      String status = (indexCreated.isActive()) ? "active" : "not active";
      indexCreatedInfo += String.format("New index: %s (%s)", indexCreatedName, status);
    }

    Index indexBackup =
        (job.getIndexBackup() != null) ? job.getIndexBackup().get(documentType) : null;
    if (indexBackup != null) {
      String indexBackupName = indexBackup.getIndexName();
      String status = (indexBackup.isActive()) ? "active" : "not active";
      String seperator = indexCreatedInfo.length() == 0 ? "" : ", ";
      indexCreatedInfo +=
          String.format("%sBackup index: %s (%s)", seperator, indexBackupName, status);
    } else {
      String seperator = indexCreatedInfo.length() == 0 ? "" : ", ";
      indexCreatedInfo += String.format("%sNo backup index", seperator);
    }

    if (job.getValidDocs(documentType) > 0) {
      infoStr =
          String.format(
              "> %s: %,d valid docs, %,d loaded, %,d deleted, %,d errors, %s  \n",
              documentType.fullName().toLowerCase(),
              job.getValidDocs(documentType),
              job.getDocumentsLoaded(documentType),
              job.getDocumentsDeleted(documentType),
              job.getTotalNumberErrors(documentType),
              indexCreatedInfo);
    }
    return infoStr;
  }

  private String getProcessErrors() {
    String msg = "";
    if (job.getStatus() == Status.ERRORS) {
      if (job.getProcessErrors() != null && job.getProcessErrors().size() > 0) {
        msg = "_error summary_:  \n";
        for (String error : job.getProcessErrors()) {
          error = error.replace("\"", "'");
          msg = msg.concat(String.format("> %s  \n", error));
        }
      }
    }
    return msg;
  }
}

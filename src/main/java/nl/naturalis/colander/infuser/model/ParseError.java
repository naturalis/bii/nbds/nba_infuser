package nl.naturalis.colander.infuser.model;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * JsonParseException.
 *
 */
public class ParseError {

  private final DocumentType documentType;
  private final String jsonDocument;
  private final String reason;

  public ParseError(DocumentType documentType, String jsonDocument, String reason) {
    this.documentType = documentType;
    this.jsonDocument = jsonDocument;
    this.reason = reason;
  }

  public DocumentType getDocumentType() {
    return documentType;
  }

  public String getJsonDocument() {
    return jsonDocument;
  }

  public String getReason() {
    return reason;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.format(
        "Error parsing document of type: %s. Original document: %s. Reason: %s",
        documentType, jsonDocument, reason);
  }
}

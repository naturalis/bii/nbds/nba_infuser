package nl.naturalis.colander.infuser.model;

/**
 * SourceSystems contributing to the Naturalis Document Store.
 */
public enum SourceSystem {
    BRAHMS,
    CRS,
    NSR,
    OBS,
    XC
}

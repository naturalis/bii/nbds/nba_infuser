package nl.naturalis.colander.infuser.model;

public enum Status {
    INPROGRESS, // Loading of the documents is in progress
    ERRORS,     // Documents have all been loaded, but there was something wrong with indexes and/or aliases
    IDLE,       // Nothing has happened (empty job file)
    FAILURE,    // There was an error loading documents
    LOADED,     // Everything went fine. All documents were loaded en index(es) were activated
    NONE       // Initial status. Infuser process hasn't started.
}

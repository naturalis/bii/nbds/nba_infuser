## properties file

# Elasticsearch
host=localhost
port=9200
scheme=http
documentType.shards=4
documentType.replicas=0

# Loader
# NOTE: the directories are the directories the application uses
# If the application is run in a container, the directories need to
# mapped to the directories in the local file system.
jobs.directory=/home/app/data/jobs
incoming.directory=/home/app/data/incoming
archive.directory=/home/app/data/archive

# Batch size refers to the (max) number of documents send to ES
# as a create, update or delete batch. 
batchSize=500

# Log server (Elasticsearch)
log.host=localhost
log.port=9200
log.scheme=http

# Slack
slack.webhook=
slack.enabled=false

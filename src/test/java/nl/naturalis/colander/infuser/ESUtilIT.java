package nl.naturalis.colander.infuser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import nl.naturalis.colander.infuser.model.Alias;
import nl.naturalis.colander.infuser.model.DocumentType;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.ResponseException;

import org.elasticsearch.client.RestHighLevelClient;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Integration Tests for ESUtil
 *
 */
public class ESUtilIT {

  // TODO: for these tests to work, (embedded) Elasticsearch needs to be implemented first
  
  private static final Logger logger = LogManager.getLogger(ESUtilIT.class);
  
  private static RestHighLevelClient client;
  private static final ObjectMapper objectMapper = new ObjectMapper();

  @BeforeClass
  public static void before() throws Exception
  {
    LoaderConfig config = LoaderConfig.getInstance();
    ESClientManager mgr = ESClientManager.newInstance(config);
    client = mgr.getClient();
  }

  @AfterClass
  public static void after() throws IOException {
    client.close();
  }
  
  @Test
  public void testClusterHealth() throws IOException
  {
    List<String> options = Arrays.asList("red", "yellow", "green");
    assertTrue("01", options.contains(ESUtil.clusterHealth(client).toLowerCase()));
  }
  
  @Test
  public void testIndexExists() throws IOException
  {
    assertTrue("01", ESUtil.indexExists(client, "taxon"));
    try
    {
      ESUtil.indexExists(client, "none");
    }
    catch (ResponseException e)
    {
      Response response = e.getResponse();
      HttpEntity entity = response.getEntity();
      String content = EntityUtils.toString(entity);
      JsonNode jsonNode = objectMapper.readTree(content);
      String type = jsonNode.get("error").get("type").asText();
      assertEquals("02", "index_not_found_exception", type);
    }
  }

  @Test
  public void testListIndices() throws IOException
  {
    assertTrue("01", Objects.requireNonNull(ESUtil.listIndices(client)).size() > 0);
  }

  @Test
  public void testCreateAndDeleteIndex() throws IOException {
    String testIndex = "specimen-test";
    assertTrue(  "01", ESUtil.createIndex(client, testIndex));
    assertFalse( "02", ESUtil.createIndex(client, testIndex));
    assertTrue(  "03", ESUtil.indexExists(client, testIndex));
    assertTrue(  "04", ESUtil.deleteIndex(client, testIndex));
    assertFalse( "05", ESUtil.indexExists(client, testIndex));
    assertFalse( "06", ESUtil.deleteIndex(client, testIndex));
  }

  @Test
  public void testAliasMethods() throws IOException
  {
    String index = "specimen-42";
    ESUtil.createIndex(client, index);

    assertNull(   "01", ESUtil.listAliases(client, index));
    assertTrue(   "02", ESUtil.addAliasToIndex(client, index, Alias.SPECIMEN));
    assertNotNull("03", ESUtil.listAliases(client, index));

    //IndexMap map = ESUtil.getIndexMap(client);
    //assertTrue( "04", map.listAllAliases(index).contains("test"));
    assertTrue( "05", ESUtil.removeAliasesFromIndex(client, index));

    ESUtil.deleteIndex(client, index);
  }

  @Test
  public void testFlushMethods() throws IOException
  {
    String index = "taxon";
    assertTrue(ESUtil.flushIndex(client, index));
    assertFalse(ESUtil.flushIndex(client, "non-existent"));
    assertTrue(ESUtil.flushAll(client));
  }

  @Test
  public void testDeleteAll() throws IOException {
    String index= "multimediaobject-crs-19700101000000";
    // ESUtil.deleteAll(client);
    ESUtil.createIndex(client, index);
    ESUtil.addAliasToIndex(client, index, Alias.MULTIMEDIA);
    String activeIndex = ESUtil.getActiveIndex(client, DocumentType.MULTIMEDIAOBJECT, "crs");
    assertEquals(index, activeIndex);
    ESUtil.deleteIndex(client, activeIndex);

  }

}

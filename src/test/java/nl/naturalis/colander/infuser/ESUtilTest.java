package nl.naturalis.colander.infuser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import nl.naturalis.colander.infuser.model.Alias;
import nl.naturalis.colander.infuser.model.DocumentType;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

public class ESUtilTest {

  @BeforeClass
  public static void before()
  {}

  @Test
  public void testGetAliasName() 
  {
    assertNull("01", ESUtil.getAlias(null));
    assertNull("02", ESUtil.getAlias("spinach"));
    assertEquals("03", Alias.SPECIMEN, ESUtil.getAlias("specimen-test"));
  }

  @Test
  public void testListIndexes() {
    try {
      System.out.println(DocumentType.forIndex("geoarea-geo-20190826153409"));
    } catch (IOException ignored) {}
  }

}
